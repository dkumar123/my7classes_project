import psycopg2

def retrive_slot_detail():
    try:
        conn = None
        conn = psycopg2.connect(
            host="localhost",
            database="Slot_booking",
            user="postgres",
            password="******")
        cur = conn.cursor()
        query = "select * from availability"
        results = cur.execute(query)
        data = cur.fetchall()
        if len(data) > 0:
            subject,slot,subject_id = slot_info_and_booking(data)
        else:
            print("No slot details available")
        st_roll = int(input("Enter Roll number to book your slot"))

        query = "select std_name from student3 where roll_no=%s"
        results = cur.execute(query,(st_roll,))
        name = cur.fetchone()

        query = "select slot_time,subject_name from slot_booking_details where roll_no=%s"
        results = cur.execute(query, (st_roll,))
        info = cur.fetchone()
        if info == None:
            booked_slot_time, booked_subject_name = None, None
        else:
            booked_slot_time, booked_subject_name = info[0],info[1]
        if name == None:
            print("Your roll number is not valid")
            return 0
        else:
            if booked_slot_time == slot:
                print("You have already another slot at this timing or You already enrolled for this subject")
            elif booked_subject_name == subject:
                print("You already enrolled for this subject")
            else:
                query = "select no_of_booked_slot,no_of_avail_slot,teacher_name from availability where subject_id =%s"
                results = cur.execute(query, (subject_id,))
                no_of_booked_slot,no_of_available_slot,teacher_name = cur.fetchone()
                if no_of_available_slot > 0:
                    no_of_booked_slot += 1
                    no_of_available_slot -= 1

                    query = "UPDATE availability SET No_of_avail_slot = %s WHERE subject_id=%s"
                    temp = (no_of_available_slot, subject_id)
                    cur.execute(query, temp)

                    query = "UPDATE availability SET No_of_booked_slot = %s WHERE subject_id=%s"
                    temp = (no_of_booked_slot, subject_id)
                    cur.execute(query, temp)

                    query = "insert into slot_booking_details values (%s,%s,%s,%s,%s)"
                    cur.execute(query, (st_roll,name,slot,teacher_name,subject))
                    conn.commit()

                    print("Your Slot is book succesfully.................")
                else:
                    print("Sorry!!!. Slot is already full")


    except(Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            cur.close()
            conn.close()
def select_subject(data):
    choice = 1
    for info in data:
        print(choice, ").\t", info[1])
        choice += 1
    print("\nSelect your slot.......  : ", end="")
    ch = int(input())
    if ch > 0 and ch < choice:
        return data[ch - 1][1]
    else:
        print("Invalid Choice!!!!!!!\nPlease select valid subject")
        return select_subject(data)

def select_slot(data,subject):
    choice = 1
    lst = []
    print("\t\t\tSlot Time\t\t\t\t\tNo. of  booked slot\t No of available slot\tTeachers Name\n")
    for info in data:
        if subject == info[1]:
            print(choice, ").\t", info[2],"\t\t\t\t",info[3],"\t\t\t\t\t",info[4],"\t\t\t\t",info[5])
            choice += 1
            lst.append((info[2],info[0]))
    print("\nSelect your slot.......  : ", end="")
    ch = int(input())
    if ch > 0 and ch < choice:
        return lst[ch - 1][0],lst[ch - 1][1]
    else:
        print("Invalid Choice!!!!!!!\nPlease select valid slot")
        return select_slot(data,subject)

def slot_info_and_booking(data):
    print("Select subject...")
    subject = select_subject(data)
    slot,subject_id = select_slot(data,subject)
    return subject,slot,subject_id

retrive_slot_detail()